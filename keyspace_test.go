package main

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseScrubRules(t *testing.T) {
	testCases := []struct {
		in    string
		test  string
		match string
	}{
		{
			in:    "foo(b)ar",
			test:  "foobar",
			match: "b",
		},
		{
			in:    "foobar",
			test:  "foobarbaz",
			match: "baz",
		},
		{
			in:    "foo(b+)ar",
			test:  "foobarfoobbar",
			match: "b",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.in, func(t *testing.T) {
			out, err := parseScrubRules([]string{tc.in})
			require.NoError(t, err)
			require.Len(t, out, 1, "number of rules")

			matches := out[0].FindAllStringSubmatch(tc.test, -1)
			require.Len(t, matches, 1, "number of matches")
			require.Len(t, matches[0], 2, "number of submatches")
			require.Equal(t, tc.match, matches[0][1], "submatch")
		})
	}
}

func TestScrub(t *testing.T) {
	testCases := []struct {
		desc  string
		in    string
		out   string
		rules []scrubRule
	}{
		{
			desc: "default: numbers",
			in:   "foo123456bar789baz1234",
			out:  "fooNUMbar789bazNUM",
		},
		{
			desc: "default: hex",
			in:   "helloe666094263world:e666094263",
			out:  "helloHEXworld:HEX",
		},
		{
			desc: "placeholder too long",
			in:   "foo123bar",
			out:  "fooABCbar",
			rules: []scrubRule{{
				placeholder: "ABCDEF",
				Regexp:      regexp.MustCompile(`([0-9]{3,})`),
			}},
		},
		{
			desc: "default: uuid",
			in:   "foo:fcdfc8aa-4370-71cb-8108-c735899f0dc9:bar:fcdfc8aa-4370-71cb-8108-c735899f0dc9",
			out:  "foo:UUID:bar:UUID",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			if len(tc.rules) == 0 {
				tc.rules = defaultScrubRules
			}
			actual := scrub([]byte(tc.in), tc.rules)
			require.Equal(t, tc.out, string(actual))
		})
	}
}
