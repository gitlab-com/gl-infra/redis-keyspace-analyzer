#!/usr/bin/env ruby
require 'redis'
require 'json'

def main
  redis = Redis.new(url: ENV['REDIS_URL'])
  $stdout.binmode

  cursor = nil
  loop do
    cursor, keys = redis.scan(cursor || '0')
    sizes = redis.pipelined { |r| keys.map { |k| r.call('memory', 'usage', k) } }
    keys.zip(sizes).each { |k, s| puts({ key: force_utf8(k), mem: s }.to_json) }

    break if cursor == '0'
  end
end

def force_utf8(str)
  str.encode('utf-8', invalid: :replace)
end

main
