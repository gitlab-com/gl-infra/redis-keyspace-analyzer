# Redis keyspace analyzer

## Usage

```
redis-keyspace-analyzer [-weighted] MAX_DEPTH MIN_WIDTH [SCRUB_RULE...]
```

There are two modes of operation:

1. Counting mode, with line-based input. Each line is treated as a key.
1. Weighted mode, with JSON-based input. Each JSON object must have a
   `key` and `weight` field. This can be used to analyze the memory use
   of different parts of the keyspace, see [weighted
   mode](#weighted-mode) below.

## Workflow

First make a dump of the Redis keys you want to analyze.

```
redis-cli --scan | gzip > key-list.gz
```

If necessary, compile the analyzer.

```
go build
```

Now run the analyzer. It may use a lot of CPU and memory so you
probably don't want to run it on a production Redis server.

```
gzcat key-list.gz | ./redis-keyspace-analyzer 20 1000
```

The first argument is the maximum prefix depth to consider. Keys
longer than this maximum depth will be truncated. The bigger this
number, the more memory the analyzer will use and the longer it will
run.

Truncating keys does not skew the statistics, but it may prevent you
from detecting patterns. A depth of 100 appears to be a reasonable
number for GitLab as of 2020Q2.

The second argument is the minimum prefix "width", that is the minimum
number of times a prefix must occur to appear in the report. This
number should be tweaked depending on how large your keyspace is and
how the keys are distributed. If you want to see all prefixes
reported, use minimum width 1. For 2020Q2 GitLab production data use
at least 1000.

When you run the command for the first time you may see results that
should be grouped together but aren't. For example `foobar:a` and
`foobar:b`. To clean up the output, add a scrub rule. This will speed
up the analysis and it helps to suppress private data from the report.

```
gzcat key-list.gz | ./redis-keyspace-analyzer 20 1000 foobar:
```

This will cause `foobar:a` and `foobar:b` to be replaced with
`foobar:X`.

A scrub rule is a [Go regular
expression](https://golang.org/pkg/regexp/syntax/). For convenience
you may omit a trailing `(.*)`; if the scrub rule contains no
parentheses the analyzer will append `(.*)` before compiling the
regular expression.

The analyzer will always prepend `^` before compiling the regular
expression.

Add scrub rules and re-run the analyzer until the report looks good. Then
increase the minimum width as needed.

### Real life scrub rule example

The following set of scrub rules was effective for analyzing about
40,000,000 Redis cache keys from gitlab.com using only 5,000,000
counters. Note that this used one specific 2020Q2 dataset. Other
(future) datasets may require other scrub rules to produce a
reasonable report.

```
gzcat key-list.gz | ./redis-keyspace-analyzer 100 1000 \
  'branch_names:' \
  'tag_names:' \
  'merged_branch_names:' \
  'cache:gitlab:root_ref:' \
  'cache:gitlab:tag_count:' \
  'cache:gitlab:size:' \
  'cache:gitlab:readme_path:' \
  'cache:gitlab:license_key:' \
  'cache:gitlab:license_blob:' \
  'cache:gitlab:has_visible_content\?:' \
  'cache:gitlab:gitlab_ci_yml:' \
  'cache:gitlab:avatar:' \
  'cache:gitlab:exists\?:' \
  'cache:gitlab:commit_count:' \
  'cache:gitlab:commit_count_' \
  'cache:gitlab:diverging_commit_counts_' \
  'cache:gitlab:avatar:' \
  'cache:gitlab:contribution_guide:' \
  'cache:gitlab:changelog:' \
  'cache:gitlab:branch_count:' \
  'cache:gitlab:can_be_resolved_in_ui\?:'
```

### Brute force

If you are so inclined you can also use brute force, instead of
feeding lots of hints to the analyzer. In this case you want to
increase the MIN_WIDTH argument until it is high enough that no
private data occurs frequently enough to reach the width threshold. In
2020Q2 the following command took about 5 minutes and used 32GB of RAM
to analyze a key dump from a gitlab.com cache Redis instance. It only
prints prefixes that occur at least 100,000 times.

```
gzcat key-list.gz | ./redis-keyspace-analyzer 100 100000
```

## Weighted mode

Instead of just counting prefixes, you can also use weighted keys. The
weight could be the memory use of the key, for instance. This allows
you to create a report of how much memory is used by the combined keys
under a certain prefix.

First create a file with a JSON object for each key along with its size.

```
ruby dump-key-info.rb | gzip > keys-info.gz
```

Then run the analyzer with the `-weighted` flag. The command below will
only print prefixes that represent 100,000,000 bytes or more of memory
usage. We use `jq` to select the weight attribute for the analyzer to use.

```
gzcat keys-info.gz | jq -c -M '{key: .key, weight: .mem}' | ./redis-keyspace-analyzer -weighted 100 100000000
```

If your key data is in JSON form but you want the output of the
regular counting mode, just feed in the keys with a fixed weight of
`1`.

```
gzcat keys-info.gz | jq -c -M '{key: .key, weight: 1}' | ./redis-keyspace-analyzer -json 100 100000000
```
